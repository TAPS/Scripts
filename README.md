The repository includes public PowerShell scripts for SharePoint (2010 + 2013) and more applications in the future. All scripts were tested for the applications/systems mentioned in the readme files. For documentation read the readme files.  
In case of problems please check first the latest version or open an Issue.

**GitLab** - https://gitlab.com/u/TobiasA       
**Twitter** - https://twitter.com/tasboeck 


   

