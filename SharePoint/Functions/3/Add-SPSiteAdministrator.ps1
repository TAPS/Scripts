
# For more details and the latest version visit https://gitlab.com/TAPS/Scripts/tree/Production/SharePoint/Functions/3 

Function Add-SPSiteAdministrator
{param([parameter(Mandatory=$true)]
[string]$Url,

[parameter(Mandatory=$true)]
[string]$Username
)

$Web = Get-SPWeb $Url -erroraction silentlycontinue

if($Web -eq $null)
    { Write-Host "The url $Url is not in the system." -foregroundcolor Red }
else { $WebUser = Get-SPUser $Username -Web $Web -erroraction silentlycontinue
        if($WebUser -eq $null)
        { try{ $WebUser = New-SPUser -UserAlias $Username -Web $Web -erroraction stop }
          catch { Write-Host "Error to add the user into site administrators, the user $Username is not valid." -foregroundcolor Red } 
        }
        if($WebUser -ne $null) 
            { $WebUser.IsSiteAdmin = $true; $WebUser.Update() }        
      }
}

