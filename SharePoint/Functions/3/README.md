Add a new site administrator to the site collection.
-
#### **Command**   
Add-SPSiteAdministrator

#### **Compatibility**  
`+` PowerShell 2  
`+` SharePoint 2010 

#### **PARAMETER Username**
The loginname of the user. The parameter is required.

#### **PARAMETER Url**
The url of the site collection. The parameter is required.

#### **Example:**   
Add-SPSiteAdministrator -Url "https://gitlab.com/TAPS/Scripts" -Username "gitlab\TobiasA"