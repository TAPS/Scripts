
# For more details and the latest version visit https://gitlab.com/TAPS/Scripts/tree/Production/SharePoint/Functions/1

function New-SPQuota
{ param([Parameter(Mandatory=$true)][string]$Name, 
    [Parameter(Mandatory=$true)][int64]$MaximumLevel, 
    [Parameter(Mandatory=$true)][int64]$WarningLevel
    )

$QuotaTemplate = New-Object Microsoft.SharePoint.Administration.SPQuotaTemplate
$QuotaTemplate.Name = $Name
$QuotaTemplate.StorageMaximumLevel = $MaximumLevel
$QuotaTemplate.StorageWarningLevel = $WarningLevel
$QuotaTemplate.UserCodeMaximumLevel = 300
$QuotaTemplate.UserCodeWarningLevel = 150
$AdminService = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
try{
$AdminService.QuotaTemplates.Add($QuotaTemplate)
$AdminService.Update()
Write-host "Quota template $Name added"
}
catch { Write-Host "Error to add the quota template $Name" -foregroundcolor Red }

}

