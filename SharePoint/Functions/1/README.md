Add a new quota template to the SharePoint central administration.
-
#### **Command**   
New-SPQuota  

#### **Compatibility**  
`+` PowerShell 2  
`+` SharePoint 2010 

#### **PARAMETER Name**
The display name of the quota template. The parameter is required.

#### **PARAMETER MaximumLevel**
The max. value of the template, in bytes. The parameter is required.

#### **PARAMETER WarningLevel**
The min. value of the template, in bytes. The parameter is required.  

#### **Example:**   
New-SPQuota -Name "Site Quota 5000 MB" -MaximumLevel 5368709120 -WarningLevel 4294967296